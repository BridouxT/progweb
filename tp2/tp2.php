<?php
$db = new PDO('mysql:host=localhost; port=3306;dbname=m3104','root','root');

    //Fichier genres.txt
    $stmt = $db->prepare("insert into genre values ( :id, :nom )");
    $fichier = file('../tp2/genres.txt');
    for($i=0; $i < count($fichier); $i++){
        $tableau = explode(";", $fichier[$i]);
        $stmt->execute(array(':id'=>$tableau[0],':nom'=>$tableau[1]));
    }

    //Fichier albums
    $stmt = $db->prepare("insert into album values ( :id, :nom, :genre, :artiste, :date )");
    $fichier = file('../tp2/albums.txt');
    for($i=0; $i < count($fichier); $i++){
        $tableau = explode(";", $fichier[$i]);
        $stmt->execute(array(':id'=>$tableau[0],':nom'=>$tableau[1],':genre'=>$tableau[2], ':artiste'=>$tableau[3], ':date'=>$tableau[4]));
    }

    //Fichiers artistes
    $stmt = $db->prepare("insert into artiste values ( :id, :nom )");
    $fichier = file('../tp2/artistes.txt');
    for($i=0; $i < count($fichier); $i++){
        $tableau = explode(";", $fichier[$i]);
        $stmt->execute(array(':id'=>$tableau[0],':nom'=>$tableau[1]));
    }
?>