<!doctype html>
<html>
    <head>
        <title>Profil</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.1/build/pure-min.css" integrity="sha384-oAOxQR6DkCoMliIh8yFnu25d7Eq/PHS21PClpwjOTeU2jRSq11vu66rf90/cZr47" crossorigin="anonymous">
    </head>
    <body>
        <?php
            session_start();
            echo "<p> </p>";
            if(array_key_exists('connected_id', $_SESSION)){
                //Recuperer info base de donnees
                $db = new PDO('mysql:host=localhost;port=3306;dbname=m3104','root','root');
                $stmt = $db->prepare("select nom, adresse_mail, departement, type_scene, annee_creation, presentation, site_web from candidature where id=:id");
                $stmt->execute(array(":id"=>$_SESSION['connected_id']));
                $result = $stmt->fetch();
                $info = array("Nom", "Adresse E-mail", "Departement", "Scene","Annee de creation", "Presentation" ,"Site Web");
                for ($nbinfo = 0; $nbinfo < 7; $nbinfo++) 
                    $value[$info[$nbinfo]] = $result[$nbinfo];
                //Recupérer nom de la scene
                $stmt = $db->prepare("select nom from scene where code=:code");
                $stmt->execute(array(":code"=>$value['Scene']));
                $result = $stmt->fetch();
                $value["Scene"] = $result[0];
                //Recuperer nom departement
                $stmt = $db->prepare("select nom from departement where num=:num");
                $stmt->execute(array(":num"=>$value['Departement']));
                $result = $stmt->fetch();
                $num = $value["Departement"];
                $value["Departement"] = $result[0] . "({$num})";

                //affichage table des resultats
                ?>
                <table class="pure-table" >
                    <thead>
                        <tr>
                            <th>Profil</th>
                            <th>   </th>
                        </tr>
                    </thead>
                    <?php
                foreach ($value as $key=>$info) {
                    echo "<tbody>
                        <tr>
                            <td>$key</td>
                            <td>$info</td>
                        </tr>
                    </tbody>";
                }
                    ?>
                </table>
                <p><a href='deconnexion.php'>Se déconnecter</a> </p>
                <?php
            }
            else{
                header("Location:index.php");
            }
        ?>
    </body>
</html>