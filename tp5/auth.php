<?php session_start(); ?>
<!doctype html>
<html>
    <head>
        <title>Authentification</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.1/build/pure-min.css" integrity="sha384-oAOxQR6DkCoMliIh8yFnu25d7Eq/PHS21PClpwjOTeU2jRSq11vu66rf90/cZr47" crossorigin="anonymous">
    </head>
    <body>
        <?php
            if(isset($_COOKIE['tp5-progweb'])){ 
                header("Location:verification.php");
            }
        ?>
        <p> </p>
        <form action="verification.php" method="POST" class="pure-form pure-form-aligned">                
            <div class='pure-control-group'>
                    <label for='nom'>Nom d'utilisateur :</label>
                    <input id='nom' type='text' name='nom' <?php if(array_key_exists('nom', $_SESSION)){ $nom = $_SESSION['nom']; echo "value=$nom";}?>>
            </div>
            <div class='pure-control-group'>
                    <label for='motdepasse'>Mot de passe :</label>
                    <input id='motdepasse' type='password' name='motdepasse'>
            </div>
            <div class="pure-controls">
                <label for="remember" class="pure-checkbox">
                    <input id="remember" type="checkbox" name='remember'> Se souvenir de moi
                </label>
                <button type="submit" class="pure-button pure-button-primary" >Connexion</button>
            </div>
        </form>
    </body>
</html>