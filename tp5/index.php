<?php session_start() ?>
<!doctype html>
<html>
    <head>
        <title>Accueil</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.1/build/pure-min.css" integrity="sha384-oAOxQR6DkCoMliIh8yFnu25d7Eq/PHS21PClpwjOTeU2jRSq11vu66rf90/cZr47" crossorigin="anonymous">
    </head>
    <body>
        <?php
            if(array_key_exists('connected_id', $_SESSION)){
                $id = $_SESSION['connected_id'];
                $db = new PDO('mysql:host=localhost;port=3306;dbname=m3104','root','root');
                $stmt = $db->prepare("select nom from candidature where id=:id");
                $stmt->execute(array(":id"=>$id));
                $result = $stmt->fetch();
                $nom = $result[0];
                echo "<p> Nom d'utilisateur : $nom</p>";
                echo "<p><a href='profil.php'>Mon profil</a> </p>";
            }
            else{
                header("Location:auth.php");
            }
        
        ?>
    </body>
</html>