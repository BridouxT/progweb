<?php
    session_start();

    //test si cookies existe
    if(isset($_COOKIE['tp5-progweb']) && isset($_COOKIE['tp5-progweb-2'])){
        $cleprive = openssl_pkey_get_private(file_get_contents('./private.pem'));
        //dechiffrage
        openssl_private_decrypt($_COOKIE['tp5-progweb'], $valeurdecrypte, $cleprive);
        openssl_private_decrypt($_COOKIE['tp5-progweb-2'], $valeurdecrypte2, $cleprive);
        //test empreinte user agent chiffre stockée dans le cookie correpond à l'empreinte du navigateur actuelle
        if($valeurdecrypte2 == $_SERVER['HTTP_USER_AGENT']){
            $_SESSION['connected_id'] = $valeurdecrypte;
            header("Location:index.php");
        }
        else{
            if(isset($_COOKIE['tp5-progweb'])){
                setcookie('tp5-progweb');
            }
            if(isset($_COOKIE['tp5-progweb-2'])){
                setcookie('tp5-progweb-2');
            }
            header("Location:auth.php");
        }
    }
    else{
        $db = new PDO('mysql:host=localhost;port=3306;dbname=m3104','root','root');
        $stmt = $db->prepare("select id, mot_de_passe from candidature where nom=:nom");
        $stmt->execute(array(":nom"=>$_POST['nom']));
        //test si nom existe dans la db
        if($result = $stmt->fetch()){
            //test password
            if(isset($result) && password_verify($_POST['motdepasse'], $result[1])){
                $_SESSION['connected_id'] = $result[0];
                //Si boite "se souvenir de moi coché"
                if(array_key_exists('remember', $_POST)){
                    $clepublic = openssl_pkey_get_public(file_get_contents('./public.pem'));
                    //chiffrement
                    openssl_public_encrypt($result[0], $valeurcrypte, $clepublic);
                    openssl_public_encrypt($_SERVER['HTTP_USER_AGENT'], $valeurcrypte2, $clepublic);
                    //cookies
                    setcookie("tp5-progweb", $valeurcrypte, time()+3600);
                    setcookie("tp5-progweb-2", $valeurcrypte2, time()+3600); 
                    
                }        
                header("Location:index.php");
            }
            else
                header("Location:auth.php");
        }
        else
            header("Location:auth.php");

    }


?>