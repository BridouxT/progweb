<?php

echo "GET :";
foreach($_GET as $variable => $valeur)
    echo "<p>$variable : $valeur</p> ";

echo "POST :";
foreach($_POST as $variable => $valeur)
    echo "<p>$variable : $valeur</p> ";
?>

<!doctype html>
<html>
    <head>
        <title>Erreurs</title>
        <meta charset="UTF-8">
    </head>
    <body>
        <?php
        if(!(filter_var($_POST["email"], FILTER_VALIDATE_EMAIL))){
            echo "Email invalide <br>";
        }
        if(strlen($_POST["motdepasse"])<7){
            echo "Mot de passe trop court < 7 <br>";
        }
        if(!(isset($_POST["scene"]))){
            echo "Scene non selectionnée <br>";
        }
        if(!(isset($_POST["condition"]))){
            echo "Case non coché <br>";
        }
        ?>
    </body>
</html>