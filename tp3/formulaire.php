<!doctype html>
<html>
    <head>
        <title>Formulaire</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.1/build/pure-min.css" integrity="sha384-oAOxQR6DkCoMliIh8yFnu25d7Eq/PHS21PClpwjOTeU2jRSq11vu66rf90/cZr47" crossorigin="anonymous">
    </head>
    <body>
        <form action="getpost.php" method="POST" class="pure-form pure-form-aligned">
            <?php
            $db = new PDO('mysql:host=localhost;port=3306;dbname=m3104','root','root');
            $indice = 0;
            $indicetableau = 0;
            $name = array("email", "motdepasse", "annee", "site");
            $label = array("Adresse E-mail", "Mot de passe", "Annee de creation", "Site Web");
            $type = array("text", "password", "text", "text");
            while($indice < 7){
                if(!($indice == 2 || $indice == 3 || $indice== 5)){
                    echo "
                    <div class='pure-control-group'>
                        <label for='$name[$indicetableau]'>$label[$indicetableau]</label>
                        <input id='$name[$indicetableau]' type='$type[$indicetableau]' name='$name[$indicetableau]'>
                    </div>
                    "; 
                    $indicetableau++;
                }
                else{
                    if($indice == 2){
                        ?>
                        <div class='pure-control-group'>
                            <label for='dep'>Numero de departement</label>
                            <select id='dep' name="dept"> 
                                <?php   
                                $stmt = $db->query("select num, nom from departement");
                                foreach($stmt as $ligne){
                                    echo "<option value='$ligne[0]'>$ligne[0] - $ligne[1]</option>"; 
                                } 
                                ?>   
                            </select>
                        </div>
                        <?php
                    }
                    elseif($indice == 3){
                        $stmt = $db->query("select code, nom from scene");
                            foreach($stmt as $ligne){
                                echo "<div class='pure-control-group'><label></label><input type='radio' name='scene' value='$ligne[0]'>$ligne[1]</div>"; 
                            } 
                    }
                    else{
                        echo "            
                        <div class='pure-control-group'>
                            <label for='presentation'>Presentation</label>
                            <textarea id='presentation' name='pres'></textarea>
                        </div>
                        ";
                    }
                }
                $indice++;
            }
  
            ?>
            <div class="pure-controls">
                    <label for="condition" class="pure-checkbox">
                        <input id="condition" type="checkbox" name='condition'> J'accepte les conditions
                    </label>

                    <button type="submit" class="pure-button pure-button-primary" value ='envoie'>Envoyer</button>
            </div>
        </form>
    </body>
</html>