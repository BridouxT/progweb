<html>
<body>

<h1>Génération de HTML conditionnelle</h1>
<?php

    $randomValue = rand(0, 100);
    if($randomValue % 2 == 0){
?>
    <p><?php echo "$randomValue";?> Valeur paire</p>
<?php
    }else{ 
?>
    <div><?php echo "$randomValue";?> Valeur impaire</div>
<?php
    }
?>


<h1>Génération HTML à partir d’une boucle</h1>
<?php
    $n = 3
?>
<select>
    <?php 
        for($i = 1; $i <= $n; $i++){
            echo "<option>Option $i</option>";
        }
    ?>
</select>

<h1>Génération HTML à partir d’une boucle 2</h1>
<table>
    <?php
        $n = 5;
        for($i = 1; $i < $n; $i++){
    ?>        
        <tr>
            <?php
                if($i % 2 == 0){                
                    echo "<td>$i</td><td>Valeur paire</td>";
                }else{ 
                    echo "<td>$i</td><td>Valeur impaire</td>";
                }
            ?>  
        </tr>
    <?php
        }
    ?>
</table>

<h1>Guillemets simples, doubles, interprétations de variables</h1>
<?php
    $titre = 'PHP "avancé"';
    $vignette = 'Z:\docs\image\oreilly.png';
    $edition = "O'Reilly";
    $prix = '$US 15';
?>
<table>    
    <tr>
        <?php echo "<td>titre</td><td>$titre</td";?>
    </tr>
    <tr>
        <?php echo "<td>vignette</td><td>$vignette</td";?>
    </tr>
    <tr>
        <?php echo "<td>titre</td><td>$edition</td";?>
    </tr>
    <tr>
        <?php echo "<td>titre</td><td>$prix</td";?>
    </tr>
</table>

<h1>Manipulation simple de chaines</h1>
<?php
    $mail = "t.bridoux@etud.u-picardie.fr";
    $positionArobase = strpos($mail, '@');
    $utilisateur = substr($mail, 0, $positionArobase);
    $nomDomaine = substr($mail, $positionArobase+1, strlen($mail));
    echo "<p>$mail</p>";
    echo "<p>$utilisateur</p>";
    echo "<p>$nomDomaine</p>";
?>


<h1>Fichiers, tableaux et boucles</h1>
<?php
    $fichier = file('../tp1/url.txt');
    for($i=0; $i < count($fichier); $i++){
        echo "<p><a href='$fichier[$i]'>$fichier[$i]<a></p>";
    }
?>


<h1>Tableaux, fichiers et découpages de chaînes</h1>
<style> .fonce{ background: #FF3388} </style>
<style> .clair{ background: #FFFFFF} </style>
<?php
    
    for($i=0; $i < count($fichier); $i++){
        if($i%2 == 0)
            $classe = "fonce";
        else
            $classe = "clair";
        $lignes = explode('#', $fichier[$i]);
        echo "<p><a  class='$classe' href='$lignes[0]'>$lignes[1]<a></p>";
    }
?>


<h1>Tableau associatif</h1>
<ul>
    <?php 
        foreach($_SERVER as $cle => $valeur)
          echo "<li>$cle => $valeur</li>";  
    ?>
</ul>

<h1>Recherche de chaine : test du navigateur</h1>
<?php
    $ua = $_SERVER['HTTP_USER_AGENT'];
    if(strpos($ua, "Firefox"))
        echo '<img src="../tp1/navigateur/firefox.png" width=50 height=50>';
    elseif(strpos($ua, "Edge"))
        echo '<img src="../tp1/navigateur/edge.png" width=50 height=50>';
    elseif(strpos($ua, "Chrome"))
        echo '<img src="../tp1/navigateur/chrome.png" width=50 height=50>';
    elseif(strpos($ua, "Safari"))
        echo '<img src="../tp1/navigateur/safari.png" width=50 height=50>';
    elseif(strpos($ua, "MSTE") or strpos($ua, "Trident"))
        echo '<img src="../tp1/navigateur/ie.png" width=50 height=50>';
    else
        echo '<p>Inconnu</p>';

    if(strpos($ua, "Windows"))
        echo '<img src="../tp1/os/windows.png" width=50 height=50>';
    elseif(strpos($ua, "Linux"))
        echo '<img src="../tp1/os/linux.png" width=50 height=50>';
    elseif(strpos($ua, "Mac"))
        echo '<img src="../tp1/os/mac.png" width=50 height=50>';
    else
        echo '<p>Inconnu</p>';



?>





</body>
</html>