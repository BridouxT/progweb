<?php
session_start();
echo "GET :";
foreach($_GET as $variable => $valeur)
    echo "<p>$variable : $valeur</p> ";

echo "POST :";
foreach($_POST as $variable => $valeur)
    echo "<p>$variable : $valeur</p> ";

?>

<!doctype html>
<html>
    <head>
        <title>Erreurs</title>
        <meta charset="UTF-8">
    </head>
    <body>
        <?php
        $db = new PDO('mysql:host=localhost;port=3306;dbname=m3104','root','root');
        if(empty($_POST['nom'])){
            $erreurs['nom'] = 'Champ nom vide';
        }
        else{
            $stmt = $db->prepare("select nom from candidature where nom=:nm");
            $stmt->execute(array(":nm"=>$_POST['nom']));
            if($stmt->rowCount() != 0){
                $erreurs['nom'] = 'Nom déjà pris';
            }
        }

        if(empty($_POST['email'])){
            $erreurs['email'] = 'Champ email vide';
        }
        else {
            if(filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)===false){
                $erreurs['email'] = 'Email invalide';
            }
        }
        
        if(empty($_POST['motdepasse'])){
            $erreurs['motdepasse'] = 'Champ mot de passe vide';
        }
        else {
            if(strlen($_POST['motdepasse'])<8){
                $erreurs['motdepasse'] = 'Mot de passe trop court';
            }
        }
        
        if(empty($_POST['annee'])){
                $erreurs['annee'] = 'Champ annee de creation vide';  
        }
        else {
            $options = array(
                'options' => array(
                    'default' => date('Y'),
                    'min_range' => 1900,
                            'max_range' => date('Y')      
                )
            );  
            if(filter_var($_POST['annee'], FILTER_VALIDATE_INT, $options)===false){
                $erreurs['annee'] = 'Champ incorrect';               
            }
        }
        
        if(empty($_POST['site'])){
            $erreurs['site'] = 'Champ site web vide';      
        }
        else {
            if(filter_var($_POST['site'],FILTER_VALIDATE_URL)===false){
                $erreurs['site'] = 'URL incorrecte'; 
            }
        }
        
        if(!isset($_POST['scene'])){
            $erreurs['scene'] = 'Aucune scene selectionnee';  
        }
        
        if(!isset($_POST['condition'])){
            $erreurs['condition'] = "Vous n'avez pas accepte les conditions"; 
        }

        foreach($erreurs as $variable => $valeur)
            echo "<p>$variable : $valeur</p> ";


        if (empty($erreurs)) {
            $stmt = $db->prepare("insert into candidature(nom, adresse_mail, mot_de_passe, departement, type_scene, annee_creation, presentation, site_web)
                                     values ( :nom, :email, :password, :dep, :scene, :annee, :pres, :site )");
            $stmt->execute(array(':nom'=>$_POST['nom'],':email'=>$_POST['email'], ':password'=>password_hash($_POST['motdepasse'], PASSWORD_DEFAULT),':dep'=>$_POST['dept'],':scene'=>$_POST['scene'],':annee'=>$_POST['annee'],':pres'=>$_POST['pres'], ':site'=>$_POST['site']));
            header("Location:success.html");
        }
        else{
            $donnees = $_POST;
            unset($donnees['motdepasse']);
            $_SESSION['donnees']=$donnees;
            $_SESSION['erreurs']=$erreurs;
            header("Location:formulaire.php");
        }

        ?>
    </body>
</html>