<!doctype html>
<html>
    <head>
        <title>Formulaire</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.1/build/pure-min.css" integrity="sha384-oAOxQR6DkCoMliIh8yFnu25d7Eq/PHS21PClpwjOTeU2jRSq11vu66rf90/cZr47" crossorigin="anonymous">
    </head>
    <body>
        <form action="getpost.php" method="POST" class="pure-form pure-form-aligned">
            <?php
            session_start();
            $db = new PDO('mysql:host=localhost;port=3306;dbname=m3104','root','root');
            $erreurs = array();
            $donnees = array();
            if(array_key_exists('erreurs', $_SESSION))
                $erreurs = $_SESSION['erreurs'];
            if(array_key_exists('donnees', $_SESSION))
                $donnees = $_SESSION['donnees'];
            $indice = 0;
            $indicetableau = 0;
            $name = array("nom", "email", "motdepasse", "annee", "site");
            $label = array("Nom", "Adresse E-mail", "Mot de passe", "Annee de creation", "Site Web");
            $type = array("text", "text", "password", "text", "text");

            echo "Erreurs :";
            foreach($erreurs as $variable => $valeur)
                echo "<p>$valeur</p> ";
            while($indice < 8){
                if(!($indice == 3 || $indice == 4 || $indice== 6)){
                    if(array_key_exists($name[$indicetableau], $donnees))
                        $value = $donnees[$name[$indicetableau]];
                    else
                        $value = '';         
                    echo "
                    <div class='pure-control-group'>
                        <label for='$name[$indicetableau]'>$label[$indicetableau]</label>
                        <input id='$name[$indicetableau]' value='$value' type='$type[$indicetableau]' name='$name[$indicetableau]'>
                    </div>
                    "; 
                    $indicetableau++;
                }
                else{
                    if($indice == 3){
                        ?>
                        <div class='pure-control-group'>
                            <label for='dep'>Numero de departement</label>
                            <select id='dep' name="dept"> 
                                <?php   
                                $stmt = $db->query("select num, nom from departement");
                                foreach($stmt as $ligne){
                                    if(array_key_exists('dept', $donnees) && $donnees['dept']==$ligne[0])
                                        $isSelected = 'selected';
                                    else
                                        $isSelected = '';
                                    echo "<option value='$ligne[0]' $isSelected>$ligne[0] - $ligne[1]</option>"; 
                                } 
                                ?>   
                            </select>
                        </div>
                        <?php
                    }
                    elseif($indice == 4){
                        $stmt = $db->query("select code, nom from scene");
                            foreach($stmt as $ligne){
                                if(array_key_exists('scene', $donnees) && $donnees['scene']==$ligne[0])
                                        $isChecked = 'checked';
                                    else
                                        $isChecked = '';
                                echo "<div class='pure-control-group'><label></label><input type='radio' name='scene' $isChecked value='$ligne[0]'>$ligne[1]</div>"; 
                            } 
                    }
                    else{
                        if(array_key_exists('pres', $donnees))
                            $value = $donnees['pres'];
                        else
                            $value = '';  
                        echo "            
                        <div class='pure-control-group'>
                            <label for='presentation'>Presentation</label>
                            <textarea id='presentation' name='pres'>$value</textarea>
                        </div>
                        ";
                    }
                }
                $indice++;
            }
  
            ?>
            <div class="pure-controls">
                    <label for="condition" class="pure-checkbox">
                        <input id="condition" type="checkbox" name='condition'> J'accepte les conditions
                    </label>

                    <button type="submit" class="pure-button pure-button-primary" value ='envoie'>Envoyer</button>
            </div>
        </form>
    </body>
</html>