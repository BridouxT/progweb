{* Commentaire Smarty *} 
<!doctype html> <html>     
<head>         
    <meta name="viewport" content="width=device-width, initial-scale=1">         
    <title>{$titre}</title>    
</head>     
<body>         
    <h1>Templating HTML</h1>    
    <p> Voici du code HTML dans un fichier de template.</p>    
    <p> Ici, une variable passée par PHP : {$titre} </p>    
</body> 
</html>